package site.tuluan.sp4j.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import site.tuluan.sp4j.common.core.domain.entity.SysUserEntity;

public interface SysUserMapper extends BaseMapper<SysUserEntity> {
}
