package site.tuluan.sp4j.module.sys.domain.vo;

/**
 * 登录对象
 *
 * @author tuluan
 */
public class LoginBody {
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
