package site.tuluan.sp4j.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import site.tuluan.sp4j.common.core.domain.entity.SysRoleEntity;

import java.util.Set;

/**
 * 角色 数据接口
 * @author tuluan
 */
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {
    /**
     * 查询指定用户角色集
     * @param userId 用户ID
     * @return 用户角色集
     */
    Set<String> selectRoleSetByUserId(@Param("userId") String userId);
}
