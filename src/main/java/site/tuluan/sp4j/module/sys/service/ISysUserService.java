package site.tuluan.sp4j.module.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.tuluan.sp4j.common.core.domain.entity.SysUserEntity;

public interface ISysUserService extends IService<SysUserEntity> {

    SysUserEntity getUser(String account);

    /**
     * 计算用户加密后密码
     * @param password 密码
     * @param salt 盐
     * @return 加密后密码
     */
    String calcEncryptedPassword(String password, String salt);

    /**
     * 重置密码
     * @param userEntity 用户
     * @return 结果
     */
    boolean resetPassword(SysUserEntity userEntity);
}
