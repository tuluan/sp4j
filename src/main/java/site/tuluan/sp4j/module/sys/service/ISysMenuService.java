package site.tuluan.sp4j.module.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.tuluan.sp4j.common.core.domain.entity.SysMenuEntity;

public interface ISysMenuService extends IService<SysMenuEntity> {
    /**
     * 同一目录下的菜单名是否唯一
     * @param menu 菜单信息
     * @return 是否在同名菜单
     */
    boolean menuNameExist(SysMenuEntity menu);
}
