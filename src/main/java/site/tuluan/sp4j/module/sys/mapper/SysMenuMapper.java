package site.tuluan.sp4j.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import site.tuluan.sp4j.common.core.domain.entity.SysMenuEntity;

import java.util.Set;

/**
 * 权限 数据接口
 * @author tuluan
 */
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {
    /**
     * 查询指定用户权限集
     * @param userId 用户ID
     * @return 用户权限集
     */
    Set<String> selectPermissionSetByUserId(@Param("userId") String userId);
}
