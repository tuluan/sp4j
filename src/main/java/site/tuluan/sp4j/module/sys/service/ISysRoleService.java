package site.tuluan.sp4j.module.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.tuluan.sp4j.common.core.domain.entity.SysRoleEntity;

public interface ISysRoleService extends IService<SysRoleEntity> {
}
