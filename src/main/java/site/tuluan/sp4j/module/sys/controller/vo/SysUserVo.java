package site.tuluan.sp4j.module.sys.controller.vo;

import site.tuluan.sp4j.common.core.domain.entity.SysUserEntity;

/**
 * 用户VO
 *
 * @author tuluan
 */
public class SysUserVo {
    /**
     * ID
     */
    private String id;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     * 0:未知 1:男 2:女
     */
    private String sex;

    /**
     * 头像地址
     */
    private String avatar;

    /**
     * 密码
     */
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SysUserEntity trans2SysUserEntity(SysUserEntity userEntity) {
        userEntity.setAccount(this.account);
        userEntity.setNickName(this.nickName);
        userEntity.setPhone(this.phone);
        userEntity.setEmail(this.email);
        userEntity.setSex(this.sex);
        userEntity.setAvatar(this.avatar);

        return userEntity;
    }

    public SysUserEntity trans2SysUserEntity() {
        SysUserEntity userEntity = new SysUserEntity();
        userEntity.setId(this.id);
        userEntity.setPassword(this.password);
        userEntity = trans2SysUserEntity(userEntity);

        return userEntity;
    }

    @Override
    public String toString() {
        return "SysUserEntity{" +
                "id='" + id + '\'' +
                ", account='" + account + '\'' +
                ", nickName='" + nickName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", sex='" + sex + '\'' +
                ", avatar='" + avatar + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
