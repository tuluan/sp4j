package site.tuluan.sp4j.module.sys.controller.vo;

public class PasswordVo {
    /**
     * 用户ID
     */
    private String id;

    /**
     * 新密码
     */
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
