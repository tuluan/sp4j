package site.tuluan.sp4j.module.sys.service;

import site.tuluan.sp4j.common.exception.BizException;

/**
 * 系统用户登录
 *
 * @author tuluan
 */
public interface ISysLoginService {
    /**
     * 用户登录
     * @param userName 用户名
     * @param password 密码
     * @return token
     * @throws BizException 业务异常
     */
    String login(String userName, String password) throws BizException;

    /**
     * 退出当前登录用户
     * @return 退出结果
     */
    boolean logout();
}
