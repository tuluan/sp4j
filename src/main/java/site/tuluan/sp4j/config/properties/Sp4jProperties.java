package site.tuluan.sp4j.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 项目配置属性
 *
 * @author tuluan
 */
@Component
@ConfigurationProperties(prefix = "sp4j")
public class Sp4jProperties {
}
