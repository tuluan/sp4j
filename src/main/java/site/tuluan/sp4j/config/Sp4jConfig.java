package site.tuluan.sp4j.config;

import org.springframework.context.annotation.Configuration;

/**
 * 项目配置类
 *
 * @author tuluan
 */
@Configuration
public class Sp4jConfig {

    /**
     * 文件上传路径
     */
    private static String fileUploadPath;

    public static String getFileUploadPath() {
        return fileUploadPath;
    }

    public static void setFileUploadPath(String fileUploadPath) {
        Sp4jConfig.fileUploadPath = fileUploadPath;
    }
}
