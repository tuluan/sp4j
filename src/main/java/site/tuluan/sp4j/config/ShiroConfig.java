package site.tuluan.sp4j.config;

import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import site.tuluan.sp4j.common.core.component.shiro.filter.JwtFilter;
import site.tuluan.sp4j.common.core.component.shiro.realm.JwtCredentialsMatcher;
import site.tuluan.sp4j.common.core.component.shiro.realm.Sp4jRealm;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro配置
 */
@Configuration
public class ShiroConfig {

    /**
     * 自定义身份认证realm
     */
    @Bean
    public Sp4jRealm sp4jRealm(JwtCredentialsMatcher matcher) {
        Sp4jRealm realm = new Sp4jRealm();
        realm.setCredentialsMatcher(matcher);
        return realm;
    }

    /**
     * 安全管理器
     */
    @Bean
    public DefaultWebSecurityManager securityManager(Sp4jRealm realm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(realm);

        // 关闭shiro自带session
        DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
        defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
        subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
        securityManager.setSubjectDAO(subjectDAO);

        return securityManager;
    }

    @Bean
    public JwtCredentialsMatcher sp4jCredentialsMatcher() {
        return new JwtCredentialsMatcher();
    }

    /**
     * JWT过滤器
     */
    public JwtFilter jwtFilter() {
        return new JwtFilter();
    }

    /**
     * Shiro过滤器配置
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // Shiro的核心安全接口，必须设置
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        // 添加jwt过滤器
        Map<String, Filter> filterMap = new LinkedHashMap<>();
        filterMap.put("authcJwt", jwtFilter());
        shiroFilterFactoryBean.setFilters(filterMap);

        // 设置拦截器 定义为LinkedMap，按添加顺序进行过滤
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        // 静态资源，允许匿名访问
        filterChainDefinitionMap.put("/favicon.ico**", "anon");
        filterChainDefinitionMap.put("/static/**", "anon");
        // 带anno标记的接口，允许匿名访问
        filterChainDefinitionMap.put("/**/anon/**", "anon");
        // 登录接口，允许匿名访问
        filterChainDefinitionMap.put("/login", "anon");
        // 退出登录接口
        filterChainDefinitionMap.put("/logout", "noSessionCreation,authcJwt[permissive]");
        // 其他接口都需要认证
        filterChainDefinitionMap.put("/**", "authcJwt");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);

        return shiroFilterFactoryBean;
    }

    /**
     *  开启Shiro的注解(如@RequiresRoles,@RequiresPermissions),需借助SpringAOP扫描使用Shiro注解的类,并在必要时进行安全逻辑验证
     * 配置以下两个bean(DefaultAdvisorAutoProxyCreator和AuthorizationAttributeSourceAdvisor)即可实现此功能
     * @return DefaultAdvisorAutoProxyCreator
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    /**
     * 开启aop注解支持
     * @param securityManager securityManager
     * @return AuthorizationAttributeSourceAdvisor
     */
    @Bean
    protected AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
}
