package site.tuluan.sp4j;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("site.tuluan.sp4j.**.mapper")
public class Sp4jApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sp4jApplication.class, args);
    }

}
