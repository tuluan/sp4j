package site.tuluan.sp4j.common.utils;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.UUID;

/**
 * 字符串工具类
 *
 * @author tuluan
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    /**
     * 去除最后一个字符
     * @param str 字符串
     * @return 新字符串
     */
    public static String delLastString(String str) {
        return delLastString(str, 1);
    }

    /**
     * 去除最后N个字符
     * <p>
     *     如果是空字符串，则返回原字符串
     *     如果去除位数超过字符串长度，则返回空字符串
     * </p>
     * @param str 字符串
     * @param num 去除位数
     * @return 新字符串
     */
    public static String delLastString(String str, int num) {
        // 如果是空字符串，则返回原字符串
        if (isEmpty(str)) {
            return str;
        }
        // 如果去除位数超过字符串长度，则返回空字符串
        if (str.length() <= num) {
            return EMPTY;
        }
        return str.substring(0, str.length() - num);
    }

    /**
     * 字符串比较，忽略字符顺序
     * @param s1 字符串1
     * @param s2 字符串2
     * @return 是否相等
     */
    public static boolean compareWithoutOrder(String s1, String s2) {
        String[] sr1 = s1.split(EMPTY);
        String[] sr2 = s2.split(EMPTY);

        Map<String, Integer> sm1 = Maps.newHashMap();
        for (String s : sr1) {
            if (sm1.containsKey(s)) {
                sm1.put(s, sm1.get(s) + 1);
            } else {
                sm1.put(s, 1);
            }
        }
        for (String s : sr2) {
            if (!sm1.containsKey(s)) {
                return false;
            }
            int i = sm1.get(s) - 1;
            if (i <= 0) {
                sm1.remove(s);
            } else {
                sm1.put(s, i);
            }
        }

        return sm1.isEmpty();
    }

    /**
     * 生成不带横线的UUID
     * @return UUID
     */
    public static String geneUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
