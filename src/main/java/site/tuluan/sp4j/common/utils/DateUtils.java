package site.tuluan.sp4j.common.utils;

import java.util.Date;

/**
 * 日期工具类
 *
 * @author tuluan
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {
    /**
     * 获取当前日期
     * @return 当前日期
     */
    public static Date getNowDate() {
        return new Date();
    }
}
