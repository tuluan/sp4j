package site.tuluan.sp4j.common.utils.web;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import site.tuluan.sp4j.common.utils.HttpUtils;
import site.tuluan.sp4j.common.utils.JsonUtils;
import site.tuluan.sp4j.common.utils.StringUtils;

/**
 * IP工具类
 *
 * @author tuluan
 */
public class IPUtils {
    private static final Logger log = LoggerFactory.getLogger(IPUtils.class);

    private static final String IP_SEARCH_URL = "http://whois.pconline.com.cn/ipJson.jsp";

    private static final String LOCAL_ADDRESS = "内网";

    /**
     * IP地址归属地查询
     * @param ip IP
     * @return 归属地
     */
    public static String getAddressByIP(String ip) {
        // 内网IP无需查询
        if (isInternalIp(ip)) {
            return LOCAL_ADDRESS;
        }

        try {
            String result = HttpUtils.sendGet(IP_SEARCH_URL, "ip=" + ip + "&json=true");
            if (StringUtils.isEmpty(result)) {
                log.error("IP地址归属地查询出错，IP:{}", ip);
                return StringUtils.EMPTY;
            }

            JsonNode resultNode = JsonUtils.jsonStr2JsonNode(result);
            String province = resultNode.get("pro").asText();
            String city = resultNode.get("city").asText();
            return String.format("%s %s", province, city);
        } catch (Exception e) {
            log.error("IP地址归属地查询出错，IP:" + ip, e);
            return StringUtils.EMPTY;
        }
    }

    /**
     * 判断是否内网IP
     * @param ip IP
     * @return 判定结果
     */
    public static boolean isInternalIp(String ip) {
        // A类 10.0.0.0/8：10.0.0.0～10.255.255.255
        // B类 172.16.0.0/12：172.16.0.0～172.31.255.255
        // C类 192.168.0.0/16：192.168.0.0～192.168.255.255
        return isInternalIp(textToNumericFormatV4(ip)) || "127.0.0.1".equals(ip);
    }

    private static boolean isInternalIp(byte[] addr) {
        if (addr == null || addr.length < 2) {
            return true;
        }
        final byte b0 = addr[0];
        final byte b1 = addr[1];
        // 10.x.x.x/8
        final byte SECTION_1 = 0x0A;
        // 172.16.x.x/12
        final byte SECTION_2 = (byte) 0xAC;
        final byte SECTION_3 = (byte) 0x10;
        final byte SECTION_4 = (byte) 0x1F;
        // 192.168.x.x/16
        final byte SECTION_5 = (byte) 0xC0;
        final byte SECTION_6 = (byte) 0xA8;
        switch (b0) {
            case SECTION_1:
                return true;
            case SECTION_2:
                if (b1 >= SECTION_3 && b1 <= SECTION_4) {
                    return true;
                }
            case SECTION_5:
                if (b1 == SECTION_6) {
                    return true;
                }
            default:
                return false;
        }
    }

    /**
     * 将IPv4地址转换成字节
     * @param text IPv4地址
     * @return byte 字节
     */
    public static byte[] textToNumericFormatV4(String text) {
        if (text.length() == 0) {
            return null;
        }

        byte[] bytes = new byte[4];
        String[] elements = text.split("\\.", -1);
        try {
            long l;
            int i;
            switch (elements.length) {
                case 1:
                    l = Long.parseLong(elements[0]);
                    if ((l < 0L) || (l > 4294967295L)) {
                        return null;
                    }
                    bytes[0] = (byte) (int) (l >> 24 & 0xFF);
                    bytes[1] = (byte) (int) ((l & 0xFFFFFF) >> 16 & 0xFF);
                    bytes[2] = (byte) (int) ((l & 0xFFFF) >> 8 & 0xFF);
                    bytes[3] = (byte) (int) (l & 0xFF);
                    break;
                case 2:
                    l = Integer.parseInt(elements[0]);
                    if ((l < 0L) || (l > 255L)) {
                        return null;
                    }
                    bytes[0] = (byte) (int) (l & 0xFF);
                    l = Integer.parseInt(elements[1]);
                    if ((l < 0L) || (l > 16777215L)) {
                        return null;
                    }
                    bytes[1] = (byte) (int) (l >> 16 & 0xFF);
                    bytes[2] = (byte) (int) ((l & 0xFFFF) >> 8 & 0xFF);
                    bytes[3] = (byte) (int) (l & 0xFF);
                    break;
                case 3:
                    for (i = 0; i < 2; ++i) {
                        l = Integer.parseInt(elements[i]);
                        if ((l < 0L) || (l > 255L)) {
                            return null;
                        }
                        bytes[i] = (byte) (int) (l & 0xFF);
                    }
                    l = Integer.parseInt(elements[2]);
                    if ((l < 0L) || (l > 65535L)) {
                        return null;
                    }
                    bytes[2] = (byte) (int) (l >> 8 & 0xFF);
                    bytes[3] = (byte) (int) (l & 0xFF);
                    break;
                case 4:
                    for (i = 0; i < 4; ++i) {
                        l = Integer.parseInt(elements[i]);
                        if ((l < 0L) || (l > 255L)) {
                            return null;
                        }
                        bytes[i] = (byte) (int) (l & 0xFF);
                    }
                    break;
                default:
                    return null;
            }
        } catch (NumberFormatException e) {
            return null;
        }
        return bytes;
    }
}
