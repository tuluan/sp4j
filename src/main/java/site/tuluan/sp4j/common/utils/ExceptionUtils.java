package site.tuluan.sp4j.common.utils;

/**
 * 异常处理工具类
 *
 * @author tuluan
 */
public class ExceptionUtils extends org.apache.commons.lang3.exception.ExceptionUtils {

    /**
     * 获取根异常中的信息
     * @param e 异常
     * @return 根异常中的信息
     */
    public static String getRootExceptionMessage(Exception e) {
        Throwable t = getRootCause(e);
        if (t == null) {
            return StringUtils.EMPTY;
        } else {
            return t.getMessage();
        }
    }

}
