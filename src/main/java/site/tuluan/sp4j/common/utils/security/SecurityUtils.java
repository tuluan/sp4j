package site.tuluan.sp4j.common.utils.security;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.subject.Subject;
import site.tuluan.sp4j.common.core.domain.LoginUser;
import site.tuluan.sp4j.common.core.domain.entity.SysUserEntity;

/**
 * 权限工具集
 *
 * @author tuluan
 */
public class SecurityUtils {
    /**
     * 获取当前登录用户信息
     * @return 当前用户信息
     */
    public static LoginUser getCurrentLoginUser() {
        return (LoginUser)getSubject().getPrincipal();
    }

    /**
     * 获取当前用户信息
     * @return 当前用户信息
     */
    public static SysUserEntity getCurrentSysUser() {
        LoginUser loginUser = getCurrentLoginUser();

        return loginUser.getSysUser();
    }

    /**
     * 判定是否超级管理员
     * @param userId 用户ID
     * @return true:是 false:否
     */
    public static boolean isSuperAdmin(String userId) {
        return "1".equals(userId);
    }

    /**
     * 生成随机盐
     * @return 盐
     */
    public static String randomSalt() {
        SecureRandomNumberGenerator generator =new SecureRandomNumberGenerator();
        return generator.nextBytes(3).toHex();
    }

    public static Subject getSubject() {
        return org.apache.shiro.SecurityUtils.getSubject();
    }
}
