package site.tuluan.sp4j.common.exception;

import org.apache.shiro.authz.AuthorizationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import site.tuluan.sp4j.common.core.domain.AjaxResult;

/**
 * 全局异常处理
 * 处理被系统抛出且非真正系统异常的异常
 *
 * @author tuluan
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(AuthorizationException.class)
    public AjaxResult authorizationExceptionHandler(AuthorizationException e) {
        return AjaxResult.buildFailureResult(HttpStatus.FORBIDDEN.value(), "没有权限，请联系管理员授权");
    }
}
