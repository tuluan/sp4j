package site.tuluan.sp4j.common.exception;

/**
 * 需要非NULL值异常
 *
 * @author tuluan
 */
public class RequiredNonNullException extends RuntimeException {
    public RequiredNonNullException() {
        super();
    }

    public RequiredNonNullException(String message) {
        super(message);
    }

    public RequiredNonNullException(String message, Throwable cause) {
        super(message, cause);
    }
}
