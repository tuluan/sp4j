package site.tuluan.sp4j.common.exception;

/**
 * 系统业务运行时异常类
 *
 * @author tuluan
 */
public class BizRunTimeException extends RuntimeException {
    public BizRunTimeException() {
        super();
    }

    public BizRunTimeException(String message) {
        super(message);
    }

    public BizRunTimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
