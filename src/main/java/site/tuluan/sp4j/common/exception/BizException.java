package site.tuluan.sp4j.common.exception;

/**
 * 系统业务异常类
 *
 * @author tuluan
 */
public class BizException extends Exception {
    public BizException() {
        super();
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }
}
