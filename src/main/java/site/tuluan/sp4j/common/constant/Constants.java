package site.tuluan.sp4j.common.constant;

public class Constants {

    /**
     * 系统用户密码加密算法
     */
    public static final String USER_ENCRYPT_ALGORITHM = "SHA-256";

    /**
     * 系统用户密码加密次数
     */
    public static final int USER_ENCRYPT_TIMES = 1024;
}
