package site.tuluan.sp4j.common.core.service.permission;

import java.util.Set;

/**
 * 权限接口
 *
 * @author tuluan
 */
public interface IPermissionService {
    /**
     * 获取用户所拥有的权限集
     * @param userId 用户ID
     * @return 用户所拥有的权限集
     */
    Set<String> getPermissionByUserId(String userId);
}
