package site.tuluan.sp4j.common.core.component.cache;

/**
 * 缓存接口
 *
 * @author tuluan
 */
public interface ICache {
    long DEFAULT_TIMEOUT = 3600;

    default <T> void setCache(String key, T value) {
        setCache(key, value, DEFAULT_TIMEOUT);
    }

    /**
     * 设置缓存
     * @param key key
     * @param value value
     * @param timeout 过期时间，单位秒
     */
    <T> void setCache(String key, T value, long timeout);

    /**
     * 获取缓存
     * @param key key
     * @return 缓存值
     */
    <T> T getCache(String key);

    /**
     * 是否存在缓存
     * @param key key
     * @return true:存在 false:不存在
     */
    boolean isExist(String key);

    /**
     * 获取剩余过期时间，单位秒
     * @param key key
     * @return 剩余过期时间，单位秒
     */
    long getTimeout(String key);

    /**
     * 删除缓存
     * @param key key
     */
    boolean removeCache(String key);
}
