package site.tuluan.sp4j.common.core.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import site.tuluan.sp4j.common.utils.DateUtils;
import site.tuluan.sp4j.common.utils.StringUtils;
import site.tuluan.sp4j.common.utils.security.SecurityUtils;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * Entity基类
 *
 * @author tuluan
 */
public class BaseEntity implements Serializable {
    @Serial
    private static final long serialVersionUID = -8646125318288575253L;

    /**
     * 删除状态
     */
    public static final int STATUS_DELETED = -1;

    /**
     * 锁定状态
     */
    public static final int STATUS_LOCKED = 0;

    /**
     * 正常状态
     */
    public static final int STATUS_NORMAL = 1;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新人
     */
    private String updater;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 状态
     * -1:删除 0:锁定 1:正常
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 数据插入之前的执行方法
     */
    public void preInsert() {
        // 设置创建人
        if (StringUtils.isEmpty(creater)) {
            creater = SecurityUtils.getCurrentSysUser().getId();
        }
        // 设置创建时间
        if (createTime == null) {
            createTime = DateUtils.getNowDate();
        }
        // 设置状态(默认为正常)
        if (status == null) {
            status = STATUS_NORMAL;
        }
    }

    /**
     * 数据更新之前的执行方法
     */
    public void preUpdate() {
        // 设置更新人
        if (StringUtils.isEmpty(updater)) {
            updater = SecurityUtils.getCurrentSysUser().getId();
        }
        // 设置更新时间
        if (updateTime == null) {
            updateTime = DateUtils.getNowDate();
        }
    }
}
