package site.tuluan.sp4j.common.core.service.role;

import java.util.Set;

/**
 * 角色接口
 *
 * @author tuluan
 */
public interface IRoleService {
    /**
     * 获取用户所拥有的角色集
     * @param userId 用户ID
     * @return 用户所拥有的角色集
     */
    Set<String> getRoleByUserId(String userId);
}
