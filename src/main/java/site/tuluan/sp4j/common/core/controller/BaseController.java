package site.tuluan.sp4j.common.core.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 控制层基类
 */
public class BaseController {
    /**
     * 日志记录
     */
    protected Logger log = LoggerFactory.getLogger(getClass());
}
