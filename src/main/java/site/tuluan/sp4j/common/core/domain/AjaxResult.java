package site.tuluan.sp4j.common.core.domain;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Maps;

import java.io.Serial;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * AJAX返回结果
 *
 * @author tuluan
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AjaxResult implements Serializable {
    @Serial
    private static final long serialVersionUID = 8660050349527606588L;

    /** 空数据实体 */
    public static final Map<Object, Object> EMPTY_DATA_MAP = Maps.newHashMap();
    /** 默认成功消息 */
    public static final String DEFAULT_SUCCESS_MSG = "OK";
    /** 默认失败消息 */
    public static final String DEFAULT_FAILURE_MSG = "FAIL";
    /** 接口返回code:200:处理成功 */
    public static final int CODE_SUCCESS = 200;
    /** 接口返回code:300:处理失败 */
    public static final int CODE_FAILURE = 300;

    /**
     * 返回结果状态
     * false:失败;true:成功;
     * */
    private boolean state = true;

    /**
     * 返回结果编码
     * */
    private int code;

    /**
     * 返回结果信息
     */
    private String msg;

    private Map<String, Long> page;

    /**
     * 返回时间
     * */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date time = Calendar.getInstance().getTime();

    /**
     * 返回数据,可以为空
     * */
    private Object data;

    /**
     * 初始化成功AJAX返回结果
     * */
    public AjaxResult() {
    }

    /**
     * 初始化成功AJAX返回结果
     * @param msg 返回结果信息
     */
    public AjaxResult(String msg) {
        this.msg = msg;
    }

    /**
     * 初始化AJAX返回结果
     * @param state 返回结果状态
     * @param msg 返回结果信息
     */
    public AjaxResult(boolean state, String msg) {
        this.state = state;
        this.msg = msg;
    }

    /**
     * 初始化AJAX返回结果
     * @param state 返回结果状态
     * @param msg 返回结果信息
     * @param code 返回结果编码
     */
    public AjaxResult(boolean state, String msg, int code) {
        this.state = state;
        this.msg = msg;
        this.code = code;
    }

    /**
     * 初始化AJAX返回结果
     * @param state 返回结果状态
     * @param msg 返回结果信息
     * @param code 返回结果编码
     * @param data 返回数据
     */
    public AjaxResult(boolean state, String msg, int code, Object data) {
        this.state = state;
        this.msg = msg;
        this.code = code;

        if (data instanceof IPage<?> pageInfo) {
            this.page = Maps.newHashMap();
            this.page.put("current", pageInfo.getCurrent());
            this.page.put("size", pageInfo.getSize());
            this.page.put("total", pageInfo.getTotal());
            this.data = pageInfo.getRecords();
        } else {
            this.data = data;
        }
    }

    /**
     * 构建默认成功信息
     * @return 成功信息
     */
    public static AjaxResult buildSuccessResult() {
        return new AjaxResult(true, DEFAULT_SUCCESS_MSG, CODE_SUCCESS, EMPTY_DATA_MAP);
    }

    /**
     * 构建成功信息(带data)
     * @param data 数据
     * @return 成功信息
     */
    public static AjaxResult buildSuccessResult(Object data) {
        return new AjaxResult(true, DEFAULT_SUCCESS_MSG, CODE_SUCCESS, data);
    }

    /**
     * 构建成功信息(带data、msg)
     * @param data 数据
     * @param msg 消息
     * @return 成功信息
     */
    public static AjaxResult buildSuccessResult(Object data, String msg) {
        return new AjaxResult(true, msg, CODE_SUCCESS, data);
    }

    /**
     * 构建默认失败信息
     * @return 失败信息
     */
    public static AjaxResult buildFailureResult() {
        return new AjaxResult(false, DEFAULT_FAILURE_MSG, CODE_FAILURE, EMPTY_DATA_MAP);
    }

    /**
     * 构建失败信息(带status)
     * @param code 失败码
     * @return 失败信息
     */
    public static AjaxResult buildFailureResult(int code) {
        return new AjaxResult(false, DEFAULT_FAILURE_MSG, code, EMPTY_DATA_MAP);
    }

    /**
     * 构建失败信息(带status、msg)
     * @param code 失败码
     * @param msg 消息
     * @return 失败信息
     */
    public static AjaxResult buildFailureResult(int code, String msg) {
        return new AjaxResult(false, msg, code, EMPTY_DATA_MAP);
    }

    /**
     * 构建失败信息(带status、msg、data)
     * @param code 失败码
     * @param msg 消息
     * @param data 数据
     * @return 失败信息
     */
    public static AjaxResult buildFailureResult(int code, String msg, Object data) {
        return new AjaxResult(false, msg, code, data);
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Map<String, Long> getPage() {
        return page;
    }
}
