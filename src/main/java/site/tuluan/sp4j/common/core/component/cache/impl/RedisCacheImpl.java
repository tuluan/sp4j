package site.tuluan.sp4j.common.core.component.cache.impl;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import site.tuluan.sp4j.common.core.component.cache.ICache;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Redis缓存实现类
 *
 * @author tuluan
 */
@Component
public class RedisCacheImpl implements ICache {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public <T> void setCache(String key, T value, long timeout) {
        redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }


    @SuppressWarnings({"unchecked"})
    @Override
    public <T> T getCache(String key) {
        return (T) redisTemplate.opsForValue().get(key);
    }

    @Override
    public boolean isExist(String key) {
        return Boolean.TRUE.equals(redisTemplate.hasKey(key));
    }

    @Override
    public long getTimeout(String key) {
        return Objects.requireNonNull(redisTemplate.getExpire(key, TimeUnit.SECONDS));
    }

    @Override
    public boolean removeCache(String key) {
        return Boolean.TRUE.equals(redisTemplate.delete(key));
    }
}
