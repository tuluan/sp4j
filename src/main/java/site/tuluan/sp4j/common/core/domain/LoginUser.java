package site.tuluan.sp4j.common.core.domain;

import site.tuluan.sp4j.common.core.domain.entity.SysUserEntity;

import java.io.Serial;
import java.io.Serializable;

/**
 * 已登录用户信息
 *
 * @author tuluan
 */
public class LoginUser implements Serializable {
    @Serial
    private static final long serialVersionUID = 5015331077811547657L;

    /**
     * 对应的系统用户
     */
    private SysUserEntity sysUser;

    /**
     * 用户令牌
     */
    private String token;

    public LoginUser(SysUserEntity sysUser) {
        this.sysUser = sysUser;
    }

    public SysUserEntity getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUserEntity sysUser) {
        this.sysUser = sysUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
