package site.tuluan.sp4j.common.core.component.token;

/**
 * Token处理异常
 */
public class TokenException extends RuntimeException{
    public TokenException(String message) {
        super(message);
    }

    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
