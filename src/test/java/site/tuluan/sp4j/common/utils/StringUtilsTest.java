package site.tuluan.sp4j.common.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {

    @Test
    void delLastString() {
        assertNull(StringUtils.delLastString(null));
        assertEquals(StringUtils.EMPTY, StringUtils.delLastString(StringUtils.EMPTY));
        assertEquals(StringUtils.EMPTY, StringUtils.delLastString("s"));
        assertEquals("text", StringUtils.delLastString("texts"));

        assertNull(StringUtils.delLastString(null, 2));
        assertEquals(StringUtils.EMPTY, StringUtils.delLastString(StringUtils.EMPTY, 2));
        assertEquals(StringUtils.EMPTY, StringUtils.delLastString("s", 2));
        assertEquals("tex", StringUtils.delLastString("texts", 2));
    }

    @Test
    void compareWithoutOrder() {
        String s1 = "test text";
        String s2 = "text test";
        assertThrows(NullPointerException.class, () -> StringUtils.compareWithoutOrder(null, null));
        assertThrows(NullPointerException.class, () -> StringUtils.compareWithoutOrder(null, s2));
        assertThrows(NullPointerException.class, () -> StringUtils.compareWithoutOrder(s1, null));

        assertFalse(StringUtils.compareWithoutOrder(s1, StringUtils.EMPTY));
        assertFalse(StringUtils.compareWithoutOrder(StringUtils.EMPTY, s2));

        assertTrue(StringUtils.compareWithoutOrder(s1, s2));
        String s3 = "test text";
        assertTrue(StringUtils.compareWithoutOrder(s1, s3));

        s3 = "a " + s3;
        assertFalse(StringUtils.compareWithoutOrder(s1, s3));
        String s4 = "a test text";
        assertTrue(StringUtils.compareWithoutOrder(s4, s3));
        s4 += "a";
        assertFalse(StringUtils.compareWithoutOrder(s4, s3));
    }
}