package site.tuluan.sp4j.common.utils;

import com.google.common.collect.Maps;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HttpUtilsTest {

    @Test
    void sendGet() {
        String url = "https://www.baidu.com";
        assertNotNull(HttpUtils.sendGet(url));

        url = "https://www.baidu.com/s";
        assertNotNull(HttpUtils.sendGet(url, "wd=spring"));
    }

    @Disabled
    @Test
    void sendGetWithBody() {
        String url = "http://localhost:8084/api/menjin/states";

        Map<String, Object> paramMap = Maps.newHashMap();
//        paramMap.put("doorIndexCodes", new String[]{"70368807eaed4c11b7bc4a783b5cd81f", "ea2e5c9d7f53481098c77bf794e7c02e"});
        String result = HttpUtils.sendGetWithBody(url, paramMap);
        assertNotNull(result);
    }

    @Disabled
    @Test
    void sendPost() {
        String url = "http://localhost:8084/api/menjin/doControl";

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("doorIndexCodes", new String[]{"70368807eaed4c11b7bc4a783b5cd81f"});
        paramMap.put("controlType", 1);

        String result = HttpUtils.sendPost(url, paramMap);
        assertNotNull(result);
    }
}