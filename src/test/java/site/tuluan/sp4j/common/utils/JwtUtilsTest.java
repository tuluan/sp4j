package site.tuluan.sp4j.common.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class JwtUtilsTest {

    @Test
    void geneToken() {
        String secret = "secret_text";

        Map<String, String> payLoadMap = Maps.newHashMap();
        payLoadMap.put("userId", "aaaaaaa");
        payLoadMap.put("name", "申");

        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJuYW1lIjoi55SzIiwidXNlcklkIjoiYWFhYWFhYSJ9.XUx0K3SSPhXHOLjav_5UekU4hzX0Was4P4T6uDqKMDv8tx4hGDndL8PxAPhegF9CU5NxfkOd-NlT9m4dJRELWQ";
        assertEquals(token, JwtUtils.geneToken(payLoadMap, secret));
        assertEquals(token, JwtUtils.geneToken(payLoadMap, secret, null));

        String expToken = JwtUtils.geneToken(payLoadMap, secret, 1);
        assertNotNull(expToken);
        assertNotEquals(token, expToken);
    }

    @Test
    void verifyToken() {
        String secret = "secret_text";
        String invalidSecret = "secret";

        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJuYW1lIjoi55SzIiwidXNlcklkIjoiYWFhYWFhYSJ9.XUx0K3SSPhXHOLjav_5UekU4hzX0Was4P4T6uDqKMDv8tx4hGDndL8PxAPhegF9CU5NxfkOd-NlT9m4dJRELWQ";
        assertTrue(JwtUtils.verifyToken(token, secret));
        assertFalse(JwtUtils.verifyToken(token, invalidSecret));

        token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoi55SzIiwidXNlcklkIjoiYWFhYWFhYSJ9.XUx0K3SSPhXHOLjav_5UekU4hzX0Was4P4T6uDqKMDv8tx4hGDndL8PxAPhegF9CU5NxfkOd-NlT9m4dJRELWQ";
        assertFalse(JwtUtils.verifyToken(token, secret));
    }

    @Test
    void getPayLoad() {
        String secret = "secret_text";

        Map<String, String> payLoadMap = Maps.newHashMap();
        payLoadMap.put("userId", "aaaaaaa");
        payLoadMap.put("name", "申");
        String token = JwtUtils.geneToken(payLoadMap, secret);
        assertEquals("{\"name\":\"申\",\"userId\":\"aaaaaaa\"}", JwtUtils.getPayLoad(token, secret));

        JsonNode jToken = JsonUtils.jsonStr2JsonNode(JwtUtils.getPayLoad(JwtUtils.geneToken(payLoadMap, secret, 1)));
        assertNotNull(jToken);
        assertEquals(3, jToken.size());
        assertEquals("申", jToken.get("name").asText());
        assertEquals("aaaaaaa", jToken.get("userId").asText());
        assertTrue(jToken.has("exp"));

    }

    @Test
    void getClaimStr() {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJuYW1lIjoi55SzIiwidXNlcklkIjoiYWFhYWFhYSJ9.XUx0K3SSPhXHOLjav_5UekU4hzX0Was4P4T6uDqKMDv8tx4hGDndL8PxAPhegF9CU5NxfkOd-NlT9m4dJRELWQ";
        assertEquals("aaaaaaa", JwtUtils.getClaimStr(token, "userId"));
    }
}