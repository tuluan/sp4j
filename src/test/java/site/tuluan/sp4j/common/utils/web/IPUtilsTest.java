package site.tuluan.sp4j.common.utils.web;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IPUtilsTest {

    @Test
    void getAddressByIP() {
        String localAddress = "内网";
        // 内网IP
        assertEquals(localAddress, IPUtils.getAddressByIP("127.0.0.1"));
        assertEquals(localAddress, IPUtils.getAddressByIP("172.16.0.0"));
        assertEquals(localAddress, IPUtils.getAddressByIP("172.16.10.10"));
        assertEquals(localAddress, IPUtils.getAddressByIP("192.168.0.0"));
        assertEquals(localAddress, IPUtils.getAddressByIP("192.168.0.1"));

        assertEquals("山东省 济南市", IPUtils.getAddressByIP("59.82.84.81"));
    }
}