package site.tuluan.sp4j.common.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.Test;
import site.tuluan.sp4j.common.exception.BizRunTimeException;

import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class JsonUtilsTest {

    @Test
    void obj2JsonStr() {
        Map<String, Object> objMap = Maps.newHashMap();

        objMap.put("code", "0");
        objMap.put("msg", "success");
        Map<String, Object> dataMap = Maps.newHashMap();
        objMap.put("data", dataMap);
        dataMap.put("visitorName", "sss");
        dataMap.put("receptionistId", "22482a05002f4fb48cd5d2b1d98e1a29");
        dataMap.put("picUrls", new String[] {"pic1", "pic2"});

        String jsonStr = "{\"code\":\"0\",\"msg\":\"success\",\"data\":{\"visitorName\":\"sss\",\"receptionistId\":\"22482a05002f4fb48cd5d2b1d98e1a29\",\"picUrls\":[\"pic1\",\"pic2\"]}}";
        assertTrue(StringUtils.compareWithoutOrder(jsonStr, JsonUtils.obj2JsonStr(objMap)));
    }

    @Test
    void jsonStr2JsonNode() {
        String jsonStr = "{\"code\":\"0\",\"msg\":\"success\",\"data\":{\"visitorName\":\"sss\",\"receptionistId\":\"22482a05002f4fb48cd5d2b1d98e1a29\",\"picUrls\":[\"pic1\",\"pic2\"]}}";
        JsonNode jsonNode = JsonUtils.jsonStr2JsonNode(jsonStr);
        assertNotNull(jsonNode);

        ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
        objectNode.put("code", "0");
        objectNode.put("msg", "success");
        objectNode.putObject("data")
                .put("visitorName", "sss")
                .put("receptionistId", "22482a05002f4fb48cd5d2b1d98e1a29")
                .putArray("picUrls").add("pic1").add("pic2");

        assertEquals(jsonNode, objectNode);

        String finalJsonStr = "{" + jsonStr;
        assertThrows(BizRunTimeException.class, () -> JsonUtils.jsonStr2JsonNode(finalJsonStr));
    }
}