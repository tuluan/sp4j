package site.tuluan.sp4j.common.core.component.cache;

import com.google.common.collect.Maps;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import site.tuluan.sp4j.Sp4jApplication;

import javax.annotation.Resource;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = Sp4jApplication.class)
class ICacheTest {
    @Resource
    private ICache iCache;

    @Test
    void testCache() {
        String key = "test_setCache";
        String value = "test_value";
        assertFalse(iCache.isExist(key));
        iCache.setCache(key, value);

        assertEquals(value, iCache.getCache(key));

        long timeout = iCache.getTimeout(key);
        assertTrue(3590 < timeout);
        assertTrue(3600 >= timeout);

        assertTrue(iCache.removeCache(key));
        assertNull(iCache.getCache(key));
        assertFalse(iCache.isExist(key));

        Map<String, Map<String, String>> map = Maps.newHashMap();
        Map<String, String> m1 = Maps.newHashMap();
        map.put("key1", m1);
        m1.put("key2", "value2");
        iCache.setCache(key, map, 10000);
        timeout = iCache.getTimeout(key);
        assertTrue(9990 < timeout);
        assertTrue(10000 >= timeout);
        assertEquals(map, iCache.getCache(key));
        iCache.removeCache(key);
        assertFalse(iCache.isExist(key));
    }
}