package site.tuluan.sp4j.common.core;

import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import site.tuluan.sp4j.Sp4jApplication;

@SpringBootTest(classes = Sp4jApplication.class)
public class ENCTest {

    @Autowired
    private StringEncryptor encryptor;

    @Test
    public void gene() {
        System.out.println("dbname:" + encryptor.encrypt("sp4j"));
        System.out.println("dbpassword:" + encryptor.encrypt("sp4j"));
    }
}
