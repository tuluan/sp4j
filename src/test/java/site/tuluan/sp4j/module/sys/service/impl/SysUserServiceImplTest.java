package site.tuluan.sp4j.module.sys.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import site.tuluan.sp4j.Sp4jApplication;
import site.tuluan.sp4j.common.core.domain.entity.SysUserEntity;
import site.tuluan.sp4j.module.sys.service.ISysUserService;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = Sp4jApplication.class)
class SysUserServiceImplTest {
    @Resource
    private ISysUserService sysUserService;

    @Test
    void getUser() {
        SysUserEntity user = sysUserService.getUser("18000000000");
        assertNull(user);
    }
}