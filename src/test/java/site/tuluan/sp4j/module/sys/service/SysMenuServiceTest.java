package site.tuluan.sp4j.module.sys.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import site.tuluan.sp4j.Sp4jApplication;
import site.tuluan.sp4j.common.core.domain.entity.SysMenuEntity;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = Sp4jApplication.class)
@Transactional
class SysMenuServiceTest {
    @Resource
    ISysMenuService sysMenuService;

    @Resource
    private JdbcTemplate template;

    @BeforeEach
    void initDBData() {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO `sys_menu` VALUES ('test_10', 'test_0', '测试', 'D', NULL, NULL, 'test:test:test', NULL, NULL, NULL, NULL, NULL, 1, NULL);");

        template.execute(sql.toString());
    }

    @Test
    void menuNameExist() {
        SysMenuEntity menu = new SysMenuEntity();
        menu.setName("测试");
        menu.setParentId("test_0");

        boolean isExist = sysMenuService.menuNameExist(menu);
        assertTrue(isExist);

        menu.setId("test_10");
        isExist = sysMenuService.menuNameExist(menu);
        assertFalse(isExist);

        menu.setId(null);
        menu.setName("test_新名称");
        isExist = sysMenuService.menuNameExist(menu);
        assertFalse(isExist);
    }
}